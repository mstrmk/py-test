from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

import re
from operator import itemgetter

options = Options()
options.add_argument("start-maximized")

SEARCH_Q = "testcase"     

browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

browser.get("https://yandex.ru")

form = browser.find_element(By.CSS_SELECTOR, value='[role=search]')
qField = form.find_element(By.ID, value='text')
qField.clear()
qField.send_keys('"' + SEARCH_Q + '"')
btnSubmit = form.find_element(By.CSS_SELECTOR, value='button[type=submit]')
btnSubmit.click()

resultWrapper = browser.find_element(By.ID, value="search-result")
items = {}

linkEl = resultWrapper.find_element(By.CSS_SELECTOR, ".organic__title-wrapper a.link")
link = linkEl.get_attribute('href')

browser.execute_script("window.open('about:blank');")
browser.switch_to.window(browser.window_handles[1])

browser.get(link)

countes = ["test", "case", "testcase"]

body = browser.find_element(By.TAG_NAME, value="body")
content = body.text

for word in countes:
    matches = re.findall(re.escape(word), content, re.IGNORECASE)
    print("count " + word + ": " + str(len(matches)))

browser.close()
browser.switch_to.window(browser.window_handles[0])
browser.close()
    