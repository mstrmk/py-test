from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC

from operator import itemgetter

COUNT_NUMS = 20
START_VALUE = 1
FINISH_VALUE = 10

def getElemById(browser, id):
    return browser.find_element(By.ID, value=id)

def setValue(field, value):
    field.clear()
    field.send_keys(value)

def getNums(resultField):
    value = resultField.get_attribute("value")
    return value.split("\n")[0].split(" ")

def validNums(nums):
    return [int(n) for n in nums if n != '' and int(n) >= START_VALUE and int(n) <= FINISH_VALUE]

def submit(buttonProcess):
    buttonProcess.click()
    wait = WebDriverWait(browser, 5)
    wait.until(EC.text_to_be_present_in_element((By.ID, "Messages"), "Copy down these data or cut and paste them into your application"))

options = Options()
options.add_argument("start-maximized")

browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

browser.get("https://psychicscience.org/random")

countField = getElemById(browser, "num")
startField = getElemById(browser, "st")
finishField = getElemById(browser, "en")
selectField = getElemById(browser, "rpt")
selectField = Select(getElemById(browser, "rpt"))
resultField = getElemById(browser, "output")
buttonProcess = getElemById(browser, "go")

try:
    setValue(countField, str(COUNT_NUMS))
    setValue(startField, str(START_VALUE))
    setValue(finishField, str(FINISH_VALUE))

    #case 1
    selectField.select_by_visible_text("Open Sequence")
    submit(buttonProcess)
    numbers = getNums(resultField)
    assert len(validNums(numbers)) == COUNT_NUMS, 'Case 1 error' 
    print("Case 1 success")
    
    #case 2
    selectField.select_by_visible_text("Closed Sequence")
    submit(buttonProcess)
    numbers = validNums(getNums(resultField))
    assert len(numbers) == COUNT_NUMS, 'Case 2 error' 
    for n in range(START_VALUE, FINISH_VALUE + 1):
        assert n in numbers, 'Case 2 error'  
    print("Case 2 success")

    #case 3
    uniqueCount = FINISH_VALUE - START_VALUE + 1
    setValue(countField, str(uniqueCount))
    selectField.select_by_visible_text("Unique Values")
    submit(buttonProcess)
    numbers = validNums(getNums(resultField))
    assert len(numbers) == uniqueCount, 'Case 3 error' 
    unqiueNums = []
    for num in numbers:
        assert num not in unqiueNums, 'Case 3 error' 
        unqiueNums.append(num)
    print("Case 3 success")

    print("All cases success!")
finally:
    browser.close()
