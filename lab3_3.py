from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

import re
from operator import itemgetter

options = Options()
options.add_argument("start-maximized")

# SEARCH_Q = "Тестирование поисковых систем"    
SEARCH_Q = "Тестирование систем"    

browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

browser.get("https://yandex.ru")

form = browser.find_element(By.CSS_SELECTOR, value='[role=search]')
qField = form.find_element(By.ID, value='text')
qField.clear()
qField.send_keys(SEARCH_Q)
btnSubmit = form.find_element(By.CSS_SELECTOR, value='button[type=submit]')
btnSubmit.click()

resultWrapper = browser.find_element(By.ID, value="search-result")
items = {}

links = resultWrapper.find_elements(By.CSS_SELECTOR, ".organic__title-wrapper a.link")
linksHrefs = []
for link in links:
    linksHrefs.append(link.get_attribute('href'))

print("Open sites:")
for index, href in enumerate(linksHrefs):
    tabName = "tab" + str(index)
    browser.execute_script("window.open('about:blank', '" + tabName + "');")
    browser.switch_to.window(tabName)
    browser.get(href)
    body = browser.find_element(By.TAG_NAME, value="body")
    matches = re.findall(re.escape(SEARCH_Q), body.text, re.IGNORECASE)
    items[tabName] = len(matches)
    print("#" + tabName + " " + browser.title)

for key, value in sorted(items.items(), key = itemgetter(1), reverse=True):
    browser.switch_to.window(key)
    browser.close()
    print(key + ' - ' + str(value))

browser.switch_to.window(browser.window_handles[0])
browser.close()
