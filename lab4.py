from selenium import webdriver
from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import re

# browser = webdriver.Firefox()
browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
browser.get('https://pro100blogger.com/')

posts = browser.find_elements(by=By.CLASS_NAME, value='d-md-flex.mg-posts-sec-post')
links = []
for item in posts:

    h1 = item.find_element(By.CLASS_NAME, 'mg-sec-top-post.py-3.col')
    h2 = h1.find_element(By.CLASS_NAME, 'mg-blog-category')
    tags = h2.find_elements(By.CLASS_NAME, 'newsup-categories.category-color-1')
    refs = h1.find_element(By.TAG_NAME, 'h4')

    tag_list = []
    for tag in tags:
        tag_list.append(tag.text.lower())
    ref = refs.find_element(By.TAG_NAME, 'a').get_attribute('href')

    links.append([ref, tag_list])

browser.switch_to.window(browser.window_handles[0])
browser.execute_script("window.open('');")
browser.switch_to.window(browser.window_handles[1])

tags_not_presented = False

for link in links:
    browser.get(link[0])

    post = browser.find_element(by=By.CLASS_NAME, value='mg-blog-post-box')
    content = post.find_element(by=By.TAG_NAME, value='article')

    for tag in link[1]:
        matches = re.findall(re.escape(tag), content.text, re.IGNORECASE)
        if (len(matches) == 0):
            print('Tag "' + tag + '" not presented at ' + link[0])
            tags_not_presented = True

browser.close()
browser.switch_to.window(browser.window_handles[0])
browser.close()

assert tags_not_presented == False, 'Some tags are missing'