from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

from operator import itemgetter

options = Options()
options.add_argument("start-maximized")

with open("./sites.txt") as f:
    sites = f.read().splitlines()

sites.sort(key=len)

browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
titles = {}

print("open sites:")
for index, site in enumerate(sites):
    tab = "tab" + str(index)
    browser.execute_script("window.open('about:blank', '" + tab + "');")
    browser.switch_to.window(tab)

    browser.get(site)
    titles[tab] = browser.title
    print(site + " [#" + tab + "]")

print("\r\nclose sites:")
for key, value in sorted(titles.items(), key = itemgetter(1)):
    browser.switch_to.window(key)
    browser.close();
    print(value + " [#" + key + "]")

browser.switch_to.window(browser.window_handles[0])
browser.close()

